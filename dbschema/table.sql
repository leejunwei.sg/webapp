-- -------------------------------------------------------------
-- TablePlus 3.12.2(358)
--
-- https://tableplus.com/
--
-- Database: webassess
-- Generation Time: 2021-02-06 20:08:50.9950
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;


DROP TABLE IF EXISTS `code_language`;
CREATE TABLE `code_language`
(
    `id`              bigint unsigned NOT NULL AUTO_INCREMENT,
    `name`            varchar(255)             DEFAULT NULL COMMENT 'Name of language',
    `templatekey`     varchar(255)             DEFAULT NULL COMMENT 'Template key (Default template in codelang folder',
    `iscodelang`      tinyint         NOT NULL DEFAULT '0' COMMENT '0 - is not coding language, 1 - is coding language',
    `needfilename`    tinyint         NOT NULL DEFAULT '0' COMMENT 'Define if the language requires a filename (like java)',
    `defaultFilename` varchar(255)             DEFAULT NULL COMMENT 'Default File Name',
    `compilerSupport` tinyint         NOT NULL DEFAULT '0' COMMENT 'If it is supported in compiler',
    `image`           varchar(255)             DEFAULT NULL COMMENT 'Compiler Image',
    `alternatekey`    varchar(255)             DEFAULT NULL COMMENT 'If there is alternate key (used for Ace)',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    UNIQUE KEY `template` (`templatekey`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `course_users`;
CREATE TABLE `course_users`
(
    `id`         bigint unsigned NOT NULL AUTO_INCREMENT,
    `courseid`   bigint unsigned NOT NULL COMMENT 'Course ID',
    `userid`     bigint unsigned NOT NULL COMMENT 'Student ID',
    `instructor` tinyint(1)      NOT NULL DEFAULT '0' COMMENT 'Is user an instructor of course (1- yes, 0 - no)',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `courseid` (`courseid`),
    KEY `userid` (`userid`),
    CONSTRAINT `course_users_ibfk_1` FOREIGN KEY (`courseid`) REFERENCES `courses` (`id`),
    CONSTRAINT `course_users_ibfk_2` FOREIGN KEY (`userid`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `courses`;
CREATE TABLE `courses`
(
    `id`        bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'Course ID',
    `code`      varchar(50)     NOT NULL COMMENT 'Course Code',
    `name`      text            NOT NULL COMMENT 'Course Name',
    `timestart` timestamp       NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Start of Course (in UTC)',
    `timeend`   timestamp       NULL     DEFAULT NULL COMMENT 'End of Course (in UTC)',
    `creator`   bigint unsigned NOT NULL COMMENT 'User who created (and admins) the course',
    `deleted`   tinyint(1)      NOT NULL DEFAULT '0' COMMENT '0 - if not deleted, 1 - if deleted',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `creator` (`creator`),
    CONSTRAINT `courses_ibfk_1` FOREIGN KEY (`creator`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments`
(
    `id`        bigint unsigned NOT NULL AUTO_INCREMENT,
    `shortname` varchar(8)      NOT NULL COMMENT 'Short Department Name',
    `fullname`  varchar(60)     NOT NULL COMMENT 'Full Department Name',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `feedbacks`;
CREATE TABLE `feedbacks` 
(
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `userid` bigint unsigned NOT NULL,
  `quizid` bigint unsigned NOT NULL,
  `rating` int NOT NULL,
  `feedback` varchar(310) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `FK_quizid_idx` (`quizid`),
  CONSTRAINT `FK_quizid` FOREIGN KEY (`quizid`) REFERENCES `quiz` (`id`)
) ENGINE=InnoDB 
  DEFAULT CHARSET=utf8mb4 
  COLLATE=utf8mb4_0900_ai_ci 
  COMMENT='This table stores all the survey feedbacks';

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`
(
    `id`       bigint unsigned NOT NULL AUTO_INCREMENT,
    `key`      varchar(255)    NOT NULL COMMENT 'Permission Key',
    `name`     text            NOT NULL COMMENT 'Permission Name',
    `reqpower` int             NOT NULL DEFAULT '0' COMMENT 'Req Permission level. User must be this and below to access',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `question`;
CREATE TABLE `question` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `questionbankid` bigint unsigned NOT NULL COMMENT 'Question Bank ID',
  `name` varchar(255) NOT NULL DEFAULT 'No Name' COMMENT 'Question Name (required)',
  `question` text NOT NULL COMMENT 'Question text',
  `maxscore` decimal(10,2) NOT NULL DEFAULT '1.00' COMMENT 'Max score for this question',
  `submission_limit` bigint DEFAULT NULL,
  `type` smallint NOT NULL DEFAULT '1' COMMENT 'Question Type (1-MCQ, 2-Structured, 3-Code, 4-Math)',
  `pos` int NOT NULL COMMENT 'Position in qn list for that questionbank',
  `deleted` tinyint NOT NULL DEFAULT '0' COMMENT 'Set to 1 for deleted',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `questionbankid` (`questionbankid`),
  CONSTRAINT `question_ibfk_1` FOREIGN KEY (`questionbankid`) REFERENCES `questionsbank` (`id`)
) ENGINE=InnoDB 
  DEFAULT CHARSET=utf8mb4 
  COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `question_access`;
CREATE TABLE `question_access` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `questionid` bigint unsigned NOT NULL,
  `userid` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `question_access_ibfk_1_idx` (`questionid`),
  KEY `question_access_ibfk_2_idx` (`userid`),
  CONSTRAINT `question_access_ibfk_1` FOREIGN KEY (`questionid`) REFERENCES `question` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `question_access_ibfk_2` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Permission to update or delete a question.\nOnly the user that created that question can update or delete that particular question, this table helps to facilitate that.';

DROP TABLE IF EXISTS `question_code`;
CREATE TABLE `question_code`
(
    `id`             bigint unsigned NOT NULL AUTO_INCREMENT,
    `questionid`     bigint unsigned NOT NULL COMMENT 'Link to question',
    `autocomplete`   tinyint         NOT NULL DEFAULT '0' COMMENT 'Whether to enable full autocomplete',
    `manualcomplete` tinyint         NOT NULL DEFAULT '0' COMMENT 'Whether to enable manual Autocomplete',
    `testfullsuite`  tinyint         NOT NULL DEFAULT '0' COMMENT 'Allow quiz takers to test against full test suite',
    `genericerrors`  tinyint         NOT NULL DEFAULT '0' COMMENT 'Shows generic errors instead of specific compilation/logic errors',
    `difficulty`     int             NOT NULL DEFAULT '0' COMMENT 'Question Difficulty (0 - Easy, 1 - Medium, 2 - Hard)',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `questionid` (`questionid`),
    CONSTRAINT `question_code_ibfk_1` FOREIGN KEY (`questionid`) REFERENCES `question` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `question_code_template`;
CREATE TABLE `question_code_template`
(
    `id`          bigint unsigned NOT NULL AUTO_INCREMENT,
    `questionid`  bigint unsigned NOT NULL COMMENT 'Question ID',
    `type`        varchar(120)    NOT NULL COMMENT 'Language Type',
    `template`    text COMMENT 'Template to display and render',
    `isprimary`   tinyint         NOT NULL DEFAULT '0' COMMENT 'If template is primary template',
    `filename`    varchar(255)             DEFAULT NULL COMMENT 'Filename of file (if set, no changes allowed during actual quiz)',
    `lockedlines` text COMMENT 'Locked lines in CSV',
    `hidelines`   text COMMENT 'Lines to hide in CSV',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `questionid` (`questionid`),
    CONSTRAINT `question_code_template_ibfk_1` FOREIGN KEY (`questionid`) REFERENCES `question` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `question_code_tests`;
CREATE TABLE `question_code_tests`
(
    `id`           bigint unsigned NOT NULL AUTO_INCREMENT,
    `questionid`   bigint unsigned NOT NULL COMMENT 'Question ID',
    `input`        text COMMENT 'Input for test case',
    `output`       text COMMENT 'Expected Output',
    `score`        decimal(10, 2)           DEFAULT NULL COMMENT 'Any score (only for actual)',
    `isactualcase` tinyint         NOT NULL DEFAULT '0' COMMENT '0 - sample, 1 - actual',
    `cpulimit`     int             NOT NULL DEFAULT '-1' COMMENT 'CPU Time Limit in Seconds',
    `memlimit`     int             NOT NULL DEFAULT '-1' COMMENT 'Memory Use Limit in MB',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `questionid` (`questionid`),
    CONSTRAINT `question_code_tests_ibfk_1` FOREIGN KEY (`questionid`) REFERENCES `question` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `question_mcq`;
CREATE TABLE `question_mcq`
(
    `id`            bigint unsigned NOT NULL AUTO_INCREMENT,
    `questionid`    bigint unsigned NOT NULL COMMENT 'Question ID',
    `randomanswers` tinyint(1)      NOT NULL DEFAULT '0' COMMENT 'Should we scramble answers during attempts',
    `style`         tinyint         NOT NULL DEFAULT '0' COMMENT 'Styles of answers (0 - 1,2,3; 1 - a,b,c)',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `questionid` (`questionid`),
    CONSTRAINT `question_mcq_ibfk_1` FOREIGN KEY (`questionid`) REFERENCES `question` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `question_mcq_ans`;
CREATE TABLE `question_mcq_ans`
(
    `id`         bigint unsigned NOT NULL AUTO_INCREMENT,
    `questionid` bigint unsigned          DEFAULT NULL COMMENT 'Question ID',
    `answer`     text COMMENT 'Answer',
    `iscorrect`  tinyint(1)      NOT NULL DEFAULT '0' COMMENT 'Is answer correct or not',
    `rationale`  text COMMENT 'Rationale for correct/wrong (optional)',
    `score`      decimal(10, 0)  NOT NULL DEFAULT '0' COMMENT 'Score to award if selected (can be negative or decimals)',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `questionid` (`questionid`),
    CONSTRAINT `question_mcq_ans_ibfk_1` FOREIGN KEY (`questionid`) REFERENCES `question` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `question_structured`;
CREATE TABLE `question_structured`
(
    `id`              bigint unsigned NOT NULL AUTO_INCREMENT,
    `questionid`      bigint unsigned DEFAULT NULL COMMENT 'Question ID',
    `correctFeedback` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci COMMENT 'Feedback if Correct answer',
    `wrongFeedback`   text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci COMMENT 'Feedback if wrong answer',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `questionid` (`questionid`),
    CONSTRAINT `question_structured_ibfk_1` FOREIGN KEY (`questionid`) REFERENCES `question` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `question_structured_keywords`;
CREATE TABLE `question_structured_keywords`
(
    `id`            bigint unsigned                                       NOT NULL AUTO_INCREMENT,
    `questionid`    bigint unsigned                                                DEFAULT NULL COMMENT 'Question ID',
    `keyword`       text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'Keyword/Phrases',
    `occurance`     int unsigned                                          NOT NULL DEFAULT '1' COMMENT 'Occurance to count',
    `score`         decimal(10, 2)                                                 DEFAULT NULL COMMENT 'Score per Occurance',
    `casesensitive` tinyint                                               NOT NULL DEFAULT '0' COMMENT 'Is Case Sensitive (0 - No, 1 - Yes)',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `questionid` (`questionid`),
    CONSTRAINT `question_structured_keywords_ibfk_1` FOREIGN KEY (`questionid`) REFERENCES `question` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `questionbank_access`;
CREATE TABLE `questionbank_access`
(
    `id`     bigint unsigned NOT NULL AUTO_INCREMENT,
    `bankid` bigint unsigned NOT NULL COMMENT 'Question Bank ID',
    `userid` bigint unsigned NOT NULL COMMENT 'User ID able to access question bank',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `bankid` (`bankid`),
    KEY `userid` (`userid`),
    CONSTRAINT `questionbank_access_ibfk_1` FOREIGN KEY (`bankid`) REFERENCES `questionsbank` (`id`),
    CONSTRAINT `questionbank_access_ibfk_2` FOREIGN KEY (`userid`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `questionsbank`;
CREATE TABLE `questionsbank`
(
    `id`      bigint unsigned NOT NULL AUTO_INCREMENT,
    `name`    varchar(255)    NOT NULL COMMENT 'Name of Question Bank',
    `topic`   varchar(255)             DEFAULT NULL COMMENT 'Question Topics if any',
    `purpose` text COMMENT 'Question Purpose if any',
    `creator` bigint unsigned NOT NULL COMMENT 'Creator of Question Bank',
    `deleted` tinyint(1)      NOT NULL DEFAULT '0' COMMENT '0 - if not deleted, 1 - if deleted',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `creator` (`creator`),
    CONSTRAINT `questionsbank_ibfk_1` FOREIGN KEY (`creator`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `quiz`;
CREATE TABLE `quiz`
(
    `id`              bigint unsigned NOT NULL AUTO_INCREMENT,
    `courseid`        bigint unsigned NOT NULL COMMENT 'Linked to which course',
    `name`            varchar(255)    NOT NULL COMMENT 'Name of quiz',
    `timestart`       timestamp       NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'What time the quiz starts (students can see it)',
    `timeend`         timestamp       NULL     DEFAULT NULL COMMENT 'What time the quiz ends (do not fill for permenant)',
    `timelimit`       bigint          NOT NULL DEFAULT '-1' COMMENT 'Whether there is a timelimit for attempting the quiz (-1 for no time limit)',
    `attempts`        int             NOT NULL DEFAULT '-1' COMMENT 'Number of attempts allowed for quiz by student (-1 for unlimited)',
    `creator`         bigint unsigned NOT NULL COMMENT 'Link to user who created it',
    `password`        varchar(255)             DEFAULT NULL COMMENT 'Is quiz password protected. If so need the password before generating the attempt',
    `passwordgen`     tinyint(1)      NOT NULL DEFAULT '0' COMMENT 'Should we randomly generate password for the quiz. Will implicitly enable password',
    `totalscore`      decimal(10, 2)  NOT NULL DEFAULT '-1.00' COMMENT 'Total score for the quiz (either match questions or a set score if you want bonus questions style) -1 to take from the question bank',
    `description`     text COMMENT 'Quiz Description',
    `preinstructions` text COMMENT 'Instructions to show to quiz takers before attempts',
    `quiztype`        tinyint         NOT NULL DEFAULT '0' COMMENT '0 - Quiz, 1 - Assignment, 2 - Practice',
    `deleted`         tinyint(1)      NOT NULL DEFAULT '0' COMMENT '0 - if not deleted, 1 - if deleted',
    `otptimelimit`    bigint unsigned NOT NULL DEFAULT '0' COMMENT 'Time in seconds (cannot be negative). Set to 0 for using default (defined by env var)',
    `showgrade`       tinyint(1)      NOT NULL DEFAULT '0' COMMENT 'Should grades be shown (0 - Never, 1 - After Quiz Attempt, 2 - After Quiz Closes)',
    `studentreport`   tinyint(1)      NOT NULL DEFAULT '0' COMMENT 'Should student report be shown (0 - Never, 1 - After Quiz Attempt, 2 - After Quiz Closes)',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `courseid` (`courseid`),
    KEY `creator` (`creator`),
    CONSTRAINT `quiz_ibfk_1` FOREIGN KEY (`courseid`) REFERENCES `courses` (`id`),
    CONSTRAINT `quiz_ibfk_2` FOREIGN KEY (`creator`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `quiz_attempt`;
CREATE TABLE `quiz_attempt`
(
    `id`            bigint unsigned NOT NULL AUTO_INCREMENT,
    `quizid`        bigint unsigned NOT NULL COMMENT 'ID to quiz',
    `studentid`     bigint unsigned NOT NULL COMMENT 'User making this attempt',
    `date`          timestamp       NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Time of attempt',
    `status`        smallint        NOT NULL DEFAULT '0' COMMENT 'Status of this attempt (0-Generating, 1-Started, 2-Completed and Pending Marking, 3-Marking, 4-Marked, 5- Error)',
    `score`         decimal(10, 2)           DEFAULT NULL COMMENT 'Student final score',
    `markeddate`    timestamp       NULL     DEFAULT NULL COMMENT 'Time of attempt being marked',
    `submitteddate` timestamp       NULL     DEFAULT NULL COMMENT 'Time of attempt being submitted',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `quizid` (`quizid`),
    CONSTRAINT `quiz_attempt_ibfk_1` FOREIGN KEY (`quizid`) REFERENCES `quiz` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `quiz_attempt_code`;
CREATE TABLE `quiz_attempt_code`
(
    `id`           bigint unsigned NOT NULL AUTO_INCREMENT,
    `attemptid`    bigint unsigned NOT NULL COMMENT 'Linked attempt question id',
    `language`     varchar(255)    NOT NULL COMMENT 'Linked language template key',
    `filename`     varchar(255)             DEFAULT NULL COMMENT 'File Name',
    `totaltests`   int             NOT NULL DEFAULT '0' COMMENT 'Total tests for this attempt question',
    `passedtests`  int             NOT NULL DEFAULT '0' COMMENT 'Passed tests for this attempt question',
    `failedtests`  int             NOT NULL DEFAULT '0' COMMENT 'Failed tests for this attempt question',
    `erroredtests` int             NOT NULL DEFAULT '0' COMMENT 'Errored tests for this attempt question',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `language` (`language`),
    KEY `attemptid` (`attemptid`),
    CONSTRAINT `quiz_attempt_code_ibfk_4` FOREIGN KEY (`language`) REFERENCES `code_language` (`templatekey`),
    CONSTRAINT `quiz_attempt_code_ibfk_5` FOREIGN KEY (`attemptid`) REFERENCES `quiz_attempt_qn` (`id`) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `quiz_attempt_qn`;
CREATE TABLE `quiz_attempt_qn`
(
    `id`         bigint unsigned NOT NULL AUTO_INCREMENT,
    `attemptid`  bigint unsigned NOT NULL COMMENT 'Link to attempt',
    `questionid` bigint unsigned NOT NULL COMMENT 'Link to question',
    `answer`     text COMMENT 'Answer given',
    `score`      decimal(10, 2)           DEFAULT NULL COMMENT 'Score given to this answer',
    `ismarked`   tinyint(1)      NOT NULL DEFAULT '0' COMMENT 'Has the backend marked this answer. Set 1 for marked',
    `usersubmit` tinyint         NOT NULL DEFAULT '0' COMMENT '0 - If theres answer, its saved. 1 - User specially submitted it',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `attemptid` (`attemptid`),
    KEY `questionid` (`questionid`),
    CONSTRAINT `quiz_attempt_qn_ibfk_2` FOREIGN KEY (`questionid`) REFERENCES `quiz_questions` (`id`),
    CONSTRAINT `quiz_attempt_qn_ibfk_3` FOREIGN KEY (`attemptid`) REFERENCES `quiz_attempt` (`id`) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `quiz_attempt_count`;
CREATE TABLE `quiz_attempt_count` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `quiz_attempt_id` bigint unsigned NOT NULL,
  `quiz` bigint unsigned NOT NULL,
  `questionid` bigint unsigned NOT NULL,
  `studentid` bigint unsigned NOT NULL,
  `submission_date` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `quiz_attempt_count_ibfk_1_idx` (`quiz_attempt_id`),
  KEY `quiz_attempt_count_ibfk_2_idx` (`quiz`),
  KEY `quiz_attempt_count_ibfk_3_idx` (`questionid`),
  KEY `quiz_attempt_count_ibfk_4_idx` (`studentid`),
  CONSTRAINT `quiz_attempt_count_ibfk_1` FOREIGN KEY (`quiz_attempt_id`) REFERENCES `quiz_attempt_qn` (`id`),
  CONSTRAINT `quiz_attempt_count_ibfk_2` FOREIGN KEY (`quiz`) REFERENCES `quiz` (`id`),
  CONSTRAINT `quiz_attempt_count_ibfk_3` FOREIGN KEY (`questionid`) REFERENCES `quiz_attempt_qn` (`questionid`),
  CONSTRAINT `quiz_attempt_count_ibfk_4` FOREIGN KEY (`studentid`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 
  DEFAULT CHARSET=utf8mb4 
  COLLATE=utf8mb4_0900_ai_ci 
  COMMENT='This table keeps track of the number of times user submits a question.\nPurpose is to limit number of submission to prevent flooding of the database table.';

DROP TABLE IF EXISTS `quiz_questions`;
CREATE TABLE `quiz_questions`
(
    `id`         bigint unsigned NOT NULL AUTO_INCREMENT,
    `quizid`     bigint unsigned NOT NULL COMMENT 'ID to Quiz',
    `questionid` bigint unsigned NOT NULL COMMENT 'ID to Question',
    `sectionid`  bigint unsigned NOT NULL COMMENT 'ID to Section (default to general)',
    `opt`        varchar(255)             DEFAULT NULL COMMENT 'Options (Optional)',
    `deleted`    tinyint(1)      NOT NULL DEFAULT '0' COMMENT '0 - if not deleted, 1 - if deleted',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `quizid` (`quizid`),
    KEY `questionid` (`questionid`),
    KEY `sectionid` (`sectionid`),
    CONSTRAINT `quiz_questions_ibfk_1` FOREIGN KEY (`quizid`) REFERENCES `quiz` (`id`),
    CONSTRAINT `quiz_questions_ibfk_2` FOREIGN KEY (`questionid`) REFERENCES `question` (`id`),
    CONSTRAINT `quiz_questions_ibfk_3` FOREIGN KEY (`sectionid`) REFERENCES `quiz_sections` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `quiz_sections`;
CREATE TABLE `quiz_sections`
(
    `id`            bigint unsigned NOT NULL AUTO_INCREMENT,
    `quizid`        bigint unsigned NOT NULL COMMENT 'Quiz ID',
    `name`          varchar(255)    NOT NULL DEFAULT 'default' COMMENT 'Section Name',
    `noofquestions` int             NOT NULL DEFAULT '-1' COMMENT '-1 for all questions in section',
    `secorder`      int             NOT NULL DEFAULT '-1' COMMENT 'Order of sections',
    `deleted`       tinyint(1)      NOT NULL DEFAULT '0' COMMENT '0 - if not deleted, 1 - if deleted',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `quizid` (`quizid`),
    CONSTRAINT `quiz_sections_ibfk_1` FOREIGN KEY (`quizid`) REFERENCES `quiz` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`
(
    `id`    bigint unsigned NOT NULL AUTO_INCREMENT,
    `key`   varchar(120)    NOT NULL COMMENT 'Role Key',
    `name`  text            NOT NULL COMMENT 'Role Name',
    `power` int             NOT NULL COMMENT 'Role Power Level',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`
(
    `id`         bigint unsigned NOT NULL AUTO_INCREMENT,
    `username`   varchar(32)     NOT NULL COMMENT 'User username',
    `password`   varchar(128)    NOT NULL COMMENT 'User Password',
    `salt`       varchar(16)     NOT NULL COMMENT 'Password Salt',
    `fullname`   varchar(255)    NOT NULL COMMENT 'User''s Full Name',
    `email`      varchar(255)    NOT NULL COMMENT 'User Email Address',
    `role`       bigint unsigned NOT NULL DEFAULT '1' COMMENT 'Role of user',
    `department` bigint unsigned NOT NULL COMMENT 'User department full name',
    `disabled`   tinyint(1)      NOT NULL DEFAULT '0' COMMENT 'If user is disabled',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    UNIQUE KEY `unique` (`username`) USING BTREE,
    KEY `role` (`role`),
    KEY `department` (`department`),
    CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role`) REFERENCES `roles` (`id`),
    CONSTRAINT `users_ibfk_2` FOREIGN KEY (`department`) REFERENCES `departments` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;



/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;