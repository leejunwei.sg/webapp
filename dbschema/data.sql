-- -------------------------------------------------------------
-- TablePlus 3.12.0(354)
--
-- https://tableplus.com/
--
-- Database: webassess
-- Generation Time: 2020-12-11 02:03:15.0800
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;


INSERT INTO `code_language` (`id`, `name`, `templatekey`, `iscodelang`, `needfilename`, `defaultFilename`,
                             `compilerSupport`, `image`, `alternatekey`)
VALUES ('1', 'Java', 'java', '1', '1', 'Submission.java', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/java', NULL),
       ('2', 'Batch', 'batchfile', '0', '0', 'main.bat', '0', NULL, NULL),
       ('3', 'C (GCC 8)', 'c-gcc8', '1', '0', 'main.c', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/gcc:8', 'c_cpp'),
       ('4', 'C (GCC 9)', 'c-gcc9', '1', '0', 'main.c', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/gcc:9', 'c_cpp'),
       ('5', 'C (GCC 10)', 'c-gcc10', '1', '0', 'main.c', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/gcc:10', 'c_cpp'),
       ('6', 'C (Clang/LLVM 10)', 'c-clang10', '1', '0', 'main.c', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/clang:10', 'c_cpp'),
       ('7', 'C (Clang/LLVM 11)', 'c-clang11', '1', '0', 'main.c', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/clang:11', 'c_cpp'),
       ('8', 'C (Clang/LLVM 12)', 'c', '1', '0', 'main.c', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/clang:12', 'c_cpp'),
       ('9', 'C++ (GCC 8)', 'cpp-gcc8', '1', '0', 'main.cpp', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/gcc:8', 'c_cpp'),
       ('10', 'C++ (GCC 9)', 'cpp-gcc9', '1', '0', 'main.cpp', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/gcc:9', 'c_cpp'),
       ('11', 'C++ (GCC 10)', 'cpp-gcc10', '1', '0', 'main.cpp', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/gcc:10', 'c_cpp'),
       ('12', 'C++ (Clang/LLVM 10)', 'cpp-clang10', '1', '0', 'main.cpp', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/clang:10', 'c_cpp'),
       ('13', 'C++ (Clang/LLVM 11)', 'cpp-clang11', '1', '0', 'main.cpp', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/clang:11', 'c_cpp'),
       ('14', 'C++ (Clang/LLVM 12)', 'cpp', '1', '0', 'main.cpp', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/clang:12', 'c_cpp'),
       ('15', 'C#', 'csharp', '1', '0', 'main.cs', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/csharp', NULL),
       ('16', 'Dockerfile', 'dockerfile', '0', '0', 'Dockerfile', '0', NULL, NULL),
       ('17', 'GoLang', 'go', '1', '0', 'main.go', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/golang', 'golang'),
       ('18', 'HTML', 'html', '0', '0', 'main.html', '0', NULL, NULL),
       ('19', 'JavaScript', 'javascript', '1', '0', 'main.js', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/javascript', NULL),
       ('20', 'Kotlin', 'kotlin', '1', '1', 'Submission.kt', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/kotlin', NULL),
       ('21', 'LaTeX', 'latex', '0', '0', 'main.tex', '0', NULL, NULL),
       ('22', 'Lua', 'lua', '1', '0', 'main.lua', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/lua', NULL),
       ('23', 'MATLAB', 'matlab', '0', '0', 'main.mat', '0', NULL, NULL),
       ('24', 'Objective-C', 'objectivec', '1', '0', 'main.m', '0', NULL, NULL),
       ('25', 'PHP', 'php', '1', '0', 'main.php', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/php', NULL),
       ('26', 'Powershell', 'powershell', '0', '0', 'main.ps', '0', NULL, NULL),
       ('27', 'Prolog', 'prolog', '0', '0', 'main.pl', '0', NULL, NULL),
       ('28', 'Python', 'python', '1', '0', 'main.py', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/python', NULL),
       ('29', 'Shell Script', 'sh', '0', '0', 'main.sh', '0',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/bash', NULL),
       ('30', 'SQL', 'sql', '0', '0', 'main.sql', '0', NULL, NULL),
       ('31', 'Swift', 'swift', '1', '0', 'main.swift', '1',
        'registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/swift', NULL),
       ('32', 'Terraform', 'terraform', '0', '0', 'main.tf', '0', NULL, NULL),
       ('33', 'Verilog', 'verilog', '0', '0', 'main.v', '0', NULL, NULL);

INSERT INTO `departments` (`id`, `shortname`, `fullname`)
VALUES ('1', 'SCSE', 'School of Computer Science and Engineering'),
       ('2', 'CBE', 'School of Chemical and Biomedical Engineering'),
       ('3', 'CEE', 'School of Civil and Environmental Engineering'),
       ('4', 'EEE', 'School of Electrical and Electronic Engineeering'),
       ('5', 'MSE', 'School of Materials Science and Engineering'),
       ('6', 'MAE', 'School of Mechanical and Aerospace Engineering'),
       ('7', 'CoE', 'College of Engineering'),
       ('8', 'NBS', 'Nanyang Buisness School'),
       ('9', 'CoHASS', 'College of Humanities, Arts and Social Sciences'),
       ('10', 'ADM', 'School of Art, Design and Media'),
       ('11', 'SOH', 'School of Humanities'),
       ('12', 'SSS', 'School of Social Sciences'),
       ('13', 'WKWSCI', 'Wee Kim Wee School of Communication and Information'),
       ('14', 'CoS', 'College of Science'),
       ('15', 'SBS', 'School of Biological Sciences'),
       ('16', 'SPMS', 'School of Physical and Mathematical Sciences'),
       ('17', 'ASE', 'Asian School of the Environment'),
       ('18', 'LKC', 'Lee Kong Chian School of Medicine'),
       ('19', 'GC', 'Graduate College'),
       ('20', 'NTU', 'Nanyang Technological University'),
       ('21', 'NIE', 'National Institute of Education');

INSERT INTO `permissions` (`id`, `key`, `name`, `reqpower`)
VALUES ('1', 'adduser', 'Add User', '9000'),
       ('2', 'edituser', 'Edit User', '9000'),
       ('3', 'deleteuser', 'Delete User', '9000'),
       ('4', 'disableuser', 'Disable User', '9000'),
       ('5', 'doquiz', 'Do Quizzes', '0'),
       ('6', 'managequestions', 'Manage Questions in Question Bank', '3000'),
       ('7', 'addquiz', 'Create Quizzes', '3000'),
       ('8', 'viewdashboard', 'View Dashboard', '0'),
       ('9', 'editquiz', 'Edit Quizzes', '3000'),
       ('10', 'viewreport', 'View Quiz Report (Individual)', '0'),
       ('11', 'viewquizreport', 'View Full Quiz Report (Teaching)', '3000'),
       ('12', 'viewallreport', 'View All Reports', '9000'),
       ('13', 'viewadmin', 'View Administration Features', '9000'),
       ('14', 'viewstudent', 'View Student Features', '0'),
       ('15', 'viewlabofficer', 'View Lab Officer Features', '1000'),
       ('16', 'viewprof', 'View Professor Features', '3000'),
       ('17', 'addcourse', 'Add Courses', '1000'),
       ('18', 'editcourse', 'Edit User to Course', '1000'),
       ('19', 'assigncourse', 'Assign User to Course', '1000'),
       ('20', 'manageallquiz', 'Manage All Quizzes', '9000'),
       ('21', 'manageallquestions', 'Manage any questions in any question bank', '9000'),
       ('22', 'multiplelogin', 'Multiple Login sessions', '1000'),
       ('23', 'viewfeedbacks', 'View All Feedbacks', '1000');

INSERT INTO `roles` (`id`, `key`, `name`, `power`)
VALUES ('1', 'student', 'Student', '100'),
       ('2', 'labofficer', 'Teaching Assistant', '1000'),
       ('3', 'instructor', 'Professor', '3000'),
       ('4', 'admin', 'Administrator', '9000'),
       ('5', 'superadmin', 'Super Admin', '9999');


/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;