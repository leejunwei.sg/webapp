const util = {};
const redis = require('./redis');
const db = require('./db');
const storage = require('./storage');
const fs = require('fs');
const formidable = require('formidable');
const nodeUtil = require('util');
const log = require('../lib/logger');

util.initialPerm = false;

util.shuffleArray = (array) => {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}

util.generateRandomChar = (length) => {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

util.refreshPermissions = async (userrole) => {
    log.info(`Refreshing ${userrole} permissions from database`);
    let [res] = await db.query("SELECT p.key FROM permissions p WHERE p.reqpower <= (SELECT r.power FROM roles r WHERE r.key = ?)", [userrole]);
    let perms = [];
    res.forEach((i) => {
        perms.push(i.key);
    });
    await redis.set(`roleperm-${userrole}`, JSON.stringify(perms), 'EX', 604800); // Expires every week
    return JSON.stringify(perms);
}

util.forceRefreshPermission = async () => {
    if (!util.initialPerm) {
        try {
            let [roleList] = await db.query("SELECT `key` FROM roles");
            roleList.forEach((role) => {
                // noinspection JSIgnoredPromiseFromCall
                util.refreshPermissions(role.key);
            });
            util.initialPerm = true;
            log.info("Refreshed Permissions");
        } catch (err) {
            log.error("DB Not found. Run /setup first");
        }
    }
}

util.getPermissionList = async (userrole) => {
    log.info(`Obtaining permission list for ${userrole}`);
    let perm = await redis.get(`roleperm-${userrole}`);
    if (perm == null) return await util.refreshPermissions(userrole);
    else return perm;
}

util.hasPermission = async (userrole, permission) => {
    log.info(`Checking for permission ${permission} for ${userrole}`);
    let perm = await util.getPermissionList(userrole);
    return perm.includes(permission);
}

util.capitalize = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

util.getCommitRev = () => {
    let env = util.getEnv();
    if (env === "development") return ` (${require('child_process').execSync('git rev-parse --short HEAD').toString().trim()})`;
    else {
        let path = 'COMMITSHA';
        if (fs.existsSync(path)) return ` (${fs.readFileSync(path, 'utf8').trimEnd()})`;
        else if (process.env.SOURCE_VERSION_SHORT) { log.info(`Found Commit SHA in ENV. Using it`); return ` (${process.env.SOURCE_VERSION_SHORT})`; }
        else {
            // Cannot find the file
            log.warn("Production and failed to find COMMITSHA in app root folder. Omitting Commit Rev");
            return "";
        }
    }
}

util.getEnv = () => {
    return process.env.NODE_ENV || "development";
}

util.getVersion = () => {
    return require('../package.json').version;
}

util.formidablePromise = (req, opts) => {
    return new Promise(function (resolve, reject) {
        let form = new formidable.IncomingForm(opts)
        form.parse(req, function (err, fields, files) {
            if (err) return reject(err)
            resolve({ fields: fields, files: files })
        })
    })
}

util.serialize = (obj) => {
    let str = [];
    for (let p in obj)
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    return str.join("&");
}

util.generateLockedLines = async (template) => {
    if (!Array.isArray(template)) {
        template = template.split('\n');
    }
    let start = [];
    let end  = [];

    template.forEach((line, i) => {
        if (line.includes("%%*")) {
            start.push(i);
        } else if (line.includes("*%%")) {
            end.push(i);
        } else if (line.includes("%%")) {
            start.push(i);
            end.push(i);
        }
    });

    start.sort();
    end.sort();

    let csvLines = "";
    let j = 0;

    start.forEach((num, i) => {
        csvLines += `${num}-`;
        if (end.length > j) {
            csvLines += `${end[j]},`;
            j++;
        } else {
            csvLines += `,`;
        }
    });

    return csvLines.replace(/,\s*$/, "");
}

util.checkLockedLines = async (qnid, qninfo) => {
    for (let tmp of qninfo.code.templates) {
        if (!tmp.locked) {
            log.warn(`No template locks found for language ${tmp.type}. Generating locked lines...`);
            let lines = await util.generateLockedLines(tmp.template);
            log.debug(`Generated Locks: ${lines}`);
            tmp.locked = lines;
            await db.query(`UPDATE question_code_template SET lockedlines=? WHERE questionid=? AND type=?`, [lines, qnid, tmp.type]);
        }
    }
    return qninfo;
}

//Submit Survey or Feedback
util.submitFeedback = async (formUserid, formQuizid, formRating, formFeedback) => {
    log.info('Attempting to submit survey feedback');
    let userid = formUserid;
    let quizid = formQuizid;
    let rating = formRating;
    let feedback = formFeedback;
    try {
        await db.query("INSERT INTO feedbacks (userid, quizid, rating, feedback) VALUES ?", [[[userid, quizid, rating, feedback]]]);
        log.info('Survey feedback successfully submitted!');
        return;
    } catch (err) {
        throw err;
    }
}

util.readFile = nodeUtil.promisify(fs.readFile);

module.exports = util;