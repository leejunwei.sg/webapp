const auth = {};
const crypto = require('crypto');
const util = require('./util');
const db = require('./db');
const cache = require('./redis');
const {miscConfigs} = require('../config');
const log = require('../lib/logger');

auth.getHashedPw = (pw, salt) => {
    return crypto.pbkdf2Sync(pw, salt, 100000, 64, 'sha512').toString('hex');
}

auth.getSalt = () => {
    return util.generateRandomChar(16);
}

auth.checkAuthentication = (sess) => {
    return !!sess.username;
}

auth.initHeader = async (req, res, renderConf) => {
    await util.forceRefreshPermission();
    renderConf.apprev = util.getCommitRev();
    renderConf.appver = util.getVersion();
    renderConf.appenv = util.getEnv();
    renderConf.config = {tinymce: miscConfigs.tinymce};
    // Check auth
    if (auth.checkAuthentication(req.session)) {
        req.session.touch(); // Refresh TTL
        renderConf.userName = req.session.fullname;
        renderConf.loginUsername = req.session.username;
        renderConf.loginId = req.session.userid;
        renderConf.userDeptShort = req.session.deptSN;
        renderConf.userDeptLong = req.session.deptFN;
        renderConf.permissions = JSON.parse(await util.getPermissionList(req.session.role));
        renderConf.hasPermission = (permission) => {return renderConf.permissions.includes(permission);};
        return true;
    } // Check authentication
    else {
        if (req.originalUrl.startsWith('/login', 0) || req.originalUrl.startsWith('/setup', 0) || req.originalUrl.startsWith('/health', 0)) return true; // Do not redirect if login page already
        log.warn('Detected Unauthenticated User');
        res.redirect('/login');
        return false;
    }
}

// If no permission, redirect to dashboard
auth.hasPermission = (req, res, renderConf, ...permission) => {
    let allowed = false;
    for (let pe of permission) {
        if (renderConf.hasPermission(pe)) {
            allowed = true;
            break;
        }
    }

    if (!allowed) {
        res.status(403).send('<meta http-equiv="refresh" content="0; url=/">');
    }
}

auth.hasPermissionBool = async (renderConf, permission) => {
    return renderConf.hasPermission(permission);
}

auth.createUser = async (formUsername, formPassword, formFullname, formEmail, formRole, formDepartment) => {
    log.info('Generating random salt and hashed password');
    let username = formUsername.toLowerCase();
    username = username.replace(/\s/g, "");
    let salt = auth.getSalt();
    let hashpw = auth.getHashedPw(formPassword, salt);
    try {
        await db.query("INSERT INTO users (username, password, salt, fullname, email, role, department) VALUES ?", [[[username,
            hashpw, salt, formFullname, formEmail, parseInt(formRole), parseInt(formDepartment)]]]);
        return username
    } catch (err) {
        throw err;
    }
}

// Clears existing session but the current one for the user
auth.clearExistingSessions = async (username, currentSession) => {
    log.debug(`Clearing all existing sessions for ${username} except for session ${currentSession}`);
    try {
        let data = await cache.keys('sess:*');
        let deleteKey = [];
        for (let sess of data) {
            let key = sess.split(':')[1];
            if (key === currentSession) continue; // Ignore current session
            let keyData = await cache.get(sess);
            keyData = JSON.parse(keyData);
            if (keyData.hasOwnProperty('username') && keyData.username === username) {
                deleteKey.push(sess);
            }
        }

        log.debug('To Delete below list');
        log.debug(deleteKey)
        if (deleteKey.length > 0) {
            await cache.del(deleteKey);
        }
    } catch (e) {
        log.error(`Failed to clear all session for user. Error: ${e}`);
    }
}

module.exports = auth;