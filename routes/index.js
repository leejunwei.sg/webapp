const express = require('express');
const router = express.Router();

const auth = require('../lib/auth');
const log = require('../lib/logger');

// Test MySQL and Redis working
const db = require('../lib/db');
const cache = require('../lib/redis');
const storage = require('../lib/storage');
const appUtil = require('../lib/util');

const renderConf = {appName: 'AASP'};

router.use('*', async (req, res, next) => { if (await auth.initHeader(req, res, renderConf)) next(); });

// Main page
router.get('/', (req, res) => {
  res.redirect('/dashboard');
});

// Login page
router.get('/login', (req, res) => {
  log.debug(req);
  log.debug(req.ip);
  log.debug(req.ips);
  let logMsg = null;
  switch (parseInt(req.query.err)) {
    case 0: logMsg = `${req.query.acct} not found in the system`; break;
    case 1: logMsg = "Invalid Password"; break;
    case 2: logMsg = "Too Many Attempts. Please try again after a few minutes"; break;
    case 3: logMsg = "Account is disabled"; break;
  }
  res.render('login', { ...renderConf, title: 'Login', arc: true, hideFooter: true, errLogin: logMsg});
});

// Login to app
router.post('/login', async (req, res) => {
  cache.rateLimit.consume(req.ip).then(async () => {
    log.debug(req.body);
    let [user] = await db.query(`SELECT u.id, u.username, u.password, u.salt, u.fullname, u.email, u.disabled, r.key, r.name, r.power, d.shortname, d.fullname as deptfullname FROM users u 
    JOIN departments d ON d.id = u.department JOIN roles r ON r.id = u.role WHERE u.username = ? LIMIT 1;`, [req.body.username.toLowerCase()]);
    log.debug(user);
    if (user.length === 0) {
      log.info("No User found");
      res.redirect(`/login?err=0&acct=${req.body.username}`);
      return;
    }
    user = user[0];
    log.info("User found");
    // Check username
    if (auth.getHashedPw(req.body.password, user.salt) !== user.password) {
      log.info("Invalid Password");
      res.redirect('/login?err=1');
      return;
    }
    // Check user disabled
    if (user.disabled) {
      log.info("Disabled user");
      res.redirect('/login?err=3');
      return;
    }
    // Clear ratelimit token
    await cache.rateLimit.delete(req.ip);
    let sess = req.session;
    // Populate sess key
    sess.username = user.username;
    sess.fullname = user.fullname;
    sess.email = user.email;
    sess.roleName = user.name;
    sess.role = user.key;
    sess.rolePower = user.power;
    sess.deptSN = user.shortname;
    sess.deptFN = user.deptfullname;
    sess.userid = user.id;
    // Get permission list just to check
    let permissionList = await appUtil.getPermissionList(req.session.role);
    if (!permissionList.includes('multiplelogin')) {
      log.info(`User has no permission for multiple login. Clearing other existing sessions`);
      auth.clearExistingSessions(user.username, sess.id).then(r => log.info(`Cleared all existing sessions for ${user.username}`));
    }
    res.redirect('/dashboard');
  }).catch(() => {
    res.redirect('/login?err=2');
  });
});

// Logout from app
router.get('/logout', (req, res) => {
  req.session.destroy(err => {
    if (err) log.error(err);
    res.redirect('/');
  });
});

// View app dashboard
router.get('/dashboard', async(req, res) => {
  log.debug(renderConf);
  let userid = renderConf.loginId;
  //get unattempted available quizzes
  let [courses] = await db.query("SELECT c.* FROM courses c JOIN course_users cu on c.id = cu.courseid WHERE cu.userid=? AND c.deleted=0 AND cu.instructor=0 AND timestart<=CURRENT_TIMESTAMP() AND (timeend IS NULL OR timeend >= CURRENT_TIMESTAMP)", userid);
  let doneQuizCount = 0;
  let availQuizCount = 0;
  for (let course of courses) {
    let [quizzes] = await db.query("SELECT * FROM quiz WHERE courseid=? AND deleted=0 AND timestart<=CURRENT_TIMESTAMP() AND (timeend IS NULL OR timeend >= CURRENT_TIMESTAMP)", course.id); // Get all quizzes within time (before start after end)
    for (let quiz of quizzes) {
      let [attempts] = await db.query("SELECT COUNT(id) as counter FROM quiz_attempt WHERE quizid=? AND studentid=?", [quiz.id, userid]);
      quiz.hasAttempt = attempts && attempts.length > 0 && attempts[0].counter > 0;
      quiz.hasAttempt?doneQuizCount++:doneQuizCount=doneQuizCount;
      if(quizzes.length > 0)
        availQuizCount++;
    }
  }
  availQuizCount = availQuizCount-doneQuizCount;
  if(availQuizCount<0)
    availQuizCount = 0;
  //get managed quizzes
  let [courses2] = await db.query("SELECT c.* FROM courses c JOIN course_users cu on c.id = cu.courseid WHERE cu.userid=? AND c.deleted=0 AND cu.instructor=1 AND timestart<=CURRENT_TIMESTAMP() AND (timeend IS NULL OR timeend >= CURRENT_TIMESTAMP)", userid);
  let managedQuizzes = [];
  let curDate = new Date();
  for (let course of courses2) {
    let [quizzes] = await db.query("SELECT * FROM quiz WHERE courseid=? AND deleted=0", course.id); // Get all quizzes in course
    let [students] = await db.query("SELECT id, userid FROM course_users WHERE courseid=? AND instructor=0", course.id);
    let studentCount = students.length; // Get students in course
    for (let quiz of quizzes) {
      quiz.isActive = true;
      if (quiz.timeend) {
        // Check if quiz has ended
        let eD = new Date(quiz.timeend);
        if (eD.getTime() - curDate.getTime() <= 0) quiz.isActive = false;
      }
      let sD = new Date(quiz.timestart);
      log.debug('%s, %s, %s, %s, %s, %s, %s', curDate.getTime(), sD.getTime(), (curDate.getTime() - sD.getTime()), quiz.name, quiz.isActive, quiz.timeend, quiz.timestart);
      if (curDate.getTime() - sD.getTime() <= 0) quiz.isActive = false;
      if (quiz.timelimit === -1) quiz.timelimit = "Unlimited";
      if (quiz.attempts === -1) quiz.attempts = "Unlimited";
      let [donestudents] = await db.query("SELECT DISTINCT qa.studentid FROM quiz_attempt qa JOIN course_users cu ON cu.userid = qa.studentid WHERE quizid=? AND status IN (2,3,4) AND cu.instructor=0", quiz.id);
      let doneCount = donestudents.length; // Get students in course and students that has attempted the quiz at least once
      managedQuizzes.push({courseid: course.id, quizid: quiz.id, quizname: quiz.name, start: quiz.timestart, end: quiz.timeend, description: quiz.description,
        coursename: course.name, coursecode: course.code, active: quiz.isActive, totalStudents: studentCount, doneStudents: doneCount});
    }
  }
  res.render('dashboard', { ...renderConf, title: 'Dashboard', arc: true, route: 'Home', availQuizCount: availQuizCount, managedQuizCount:managedQuizzes.length});
});

// Get data for all available quiz for the user
router.get('/data/availableQuiz', async (req, res) => {
  let userid = renderConf.loginId;
  let [courses] = await db.query("SELECT c.* FROM courses c JOIN course_users cu on c.id = cu.courseid WHERE cu.userid=? AND c.deleted=0 AND cu.instructor=0 AND timestart<=CURRENT_TIMESTAMP() AND (timeend IS NULL OR timeend >= CURRENT_TIMESTAMP)", userid);
  let availQuizzes = [];
  for (let course of courses) {
    let [quizzes] = await db.query("SELECT * FROM quiz WHERE courseid=? AND deleted=0 AND timestart<=CURRENT_TIMESTAMP() AND (timeend IS NULL OR timeend >= CURRENT_TIMESTAMP)", course.id); // Get all quizzes within time (before start after end)
    for (let quiz of quizzes) {
      if (quiz.timelimit === -1) quiz.timelimit = "Unlimited";
      if (quiz.attempts === -1) quiz.attempts = "Unlimited";
      let [attempts] = await db.query("SELECT COUNT(id) as counter FROM quiz_attempt WHERE quizid=? AND studentid=?", [quiz.id, userid]);
      quiz.hasAttempt = attempts && attempts.length > 0 && attempts[0].counter > 0;
      availQuizzes.push({courseid: course.id, quizid: quiz.id, quizname: quiz.name, start: quiz.timestart, end: quiz.timeend, attempted: quiz.hasAttempt, description: quiz.description,
        coursename: course.name, coursecode: course.code});
    }
  }
  res.json({data: availQuizzes});
});

// Get data of all quizzes the user is managing
router.get('/data/managedQuiz', async (req, res) => {
  let userid = renderConf.loginId;
  let [courses] = await db.query("SELECT c.* FROM courses c JOIN course_users cu on c.id = cu.courseid WHERE cu.userid=? AND c.deleted=0 AND cu.instructor=1 AND timestart<=CURRENT_TIMESTAMP() AND (timeend IS NULL OR timeend >= CURRENT_TIMESTAMP)", userid);
  let managedQuizzes = [];
  let curDate = new Date();
  for (let course of courses) {
    let [quizzes] = await db.query("SELECT * FROM quiz WHERE courseid=? AND deleted=0", course.id); // Get all quizzes in course
    let [students] = await db.query("SELECT id, userid FROM course_users WHERE courseid=? AND instructor=0", course.id);
    let studentCount = students.length; // Get students in course
    for (let quiz of quizzes) {
      quiz.isActive = true;
      if (quiz.timeend) {
        // Check if quiz has ended
        let eD = new Date(quiz.timeend);
        if (eD.getTime() - curDate.getTime() <= 0) quiz.isActive = false;
      }
      let sD = new Date(quiz.timestart);
      log.debug('%s, %s, %s, %s, %s, %s, %s', curDate.getTime(), sD.getTime(), (curDate.getTime() - sD.getTime()), quiz.name, quiz.isActive, quiz.timeend, quiz.timestart);
      if (curDate.getTime() - sD.getTime() <= 0) quiz.isActive = false;
      if (quiz.timelimit === -1) quiz.timelimit = "Unlimited";
      if (quiz.attempts === -1) quiz.attempts = "Unlimited";
      let [donestudents] = await db.query("SELECT DISTINCT qa.studentid FROM quiz_attempt qa JOIN course_users cu ON cu.userid = qa.studentid WHERE quizid=? AND status IN (2,3,4) AND cu.instructor=0", quiz.id);
      let doneCount = donestudents.length; // Get students in course and students that has attempted the quiz at least once
      managedQuizzes.push({courseid: course.id, quizid: quiz.id, quizname: quiz.name, start: quiz.timestart, end: quiz.timeend, description: quiz.description,
        coursename: course.name, coursecode: course.code, active: quiz.isActive, totalStudents: studentCount, doneStudents: doneCount});
    }
  }
  res.json({data: managedQuizzes});
});

// Get data of all quizzes
router.get('/data/allQuiz', async (req, res) => {
  auth.hasPermission(req, res, renderConf, 'manageallquiz', 'viewallreport');
  let [courses] = await db.query("SELECT c.* FROM courses c WHERE c.deleted=0 AND timestart<=CURRENT_TIMESTAMP() AND (timeend IS NULL OR timeend >= CURRENT_TIMESTAMP)");
  let managedQuizzes = [];
  let curDate = new Date();
  for (let course of courses) {
    let [quizzes] = await db.query("SELECT * FROM quiz WHERE courseid=? AND deleted=0", course.id); // Get all quizzes in course
    let [students] = await db.query("SELECT id, userid FROM course_users WHERE courseid=? AND instructor=0", course.id);
    let studentCount = students.length; // Get students in course
    for (let quiz of quizzes) {
      quiz.isActive = true;
      if (quiz.timeend) {
        // Check if quiz has ended
        let eD = new Date(quiz.timeend);
        if (eD.getTime() - curDate.getTime() <= 0) quiz.isActive = false;
      }
      let sD = new Date(quiz.timestart);
      log.debug('%s, %s, %s, %s, %s, %s, %s', curDate.getTime(), sD.getTime(), (curDate.getTime() - sD.getTime()), quiz.name, quiz.isActive, quiz.timeend, quiz.timestart);
      if (curDate.getTime() - sD.getTime() <= 0) quiz.isActive = false;
      if (quiz.timelimit === -1) quiz.timelimit = "Unlimited";
      if (quiz.attempts === -1) quiz.attempts = "Unlimited";
      let [donestudents] = await db.query("SELECT DISTINCT qa.studentid FROM quiz_attempt qa JOIN course_users cu ON cu.userid = qa.studentid WHERE quizid=? AND status IN (2,3,4) AND cu.instructor=0", quiz.id);
      let doneCount = donestudents.length; // Get students in course and students that has attempted the quiz at least once
      managedQuizzes.push({courseid: course.id, quizid: quiz.id, quizname: quiz.name, start: quiz.timestart, end: quiz.timeend, description: quiz.description,
        coursename: course.name, coursecode: course.code, active: quiz.isActive, totalStudents: studentCount, doneStudents: doneCount});
    }
  }
  res.json({data: managedQuizzes});
});

// Change password page
router.get('/changepassword', (req, res) => {
  res.render('users/changepw', { ...renderConf, title: 'Change Password', username: req.session.username});
});

// Change password for user
router.post('/changepassword', async (req, res) => {
  log.debug(req.body);
  // Make sure password matches old password
  let username = req.session.username; // We do not use the form username to prevent ppl changing the value there
  let [oldpwdata] = await db.query(`SELECT u.password, u.salt FROM users u WHERE u.username = ? LIMIT 1;`, [username]);
  let oldHashedPw = auth.getHashedPw(req.body.password, oldpwdata[0].salt);
  if (oldHashedPw !== oldpwdata[0].password) {
    res.render('users/changepw', {...renderConf, title: 'Change Password', username: req.session.username, errmsg: "Invalid Password"});
    return;
  }
  // Change that old password does not match new password to change to
  if (req.body.password === req.body.newpw) {
    res.render('users/changepw', {...renderConf, title: 'Change Password', username: req.session.username, errmsg: "Old and new passwords cannot be identical"});
    return;
  }
  // Change your password
  let newSalt = auth.getSalt(); // Get new salt for new password
  let hashedPw = auth.getHashedPw(req.body.newpw, newSalt); // Get new hashed password
  await db.query('UPDATE users SET password=?, salt=? WHERE username = ?', [hashedPw, newSalt, username]);
  res.render('users/changepw', {...renderConf, title: 'Change Password', username: req.session.username, sucmsg: "Password has been changed successfully!"});
});

// Upload image from Tiny WYSIWYG editor
router.post('/data/editorImgUpload', async (req, res) => {
  log.info("Uploading");
  let form = await appUtil.formidablePromise(req);
  let file = form.files.file;
  log.info(file);
  let loc = await storage.uploadPublic(file, req.query.fp);
  res.json(loc);
});

// View test case how-to
router.get('/howto/testcase', async (req, res) => {
  res.render('guides/uploadtestcase', {...renderConf, title: 'How to upload test cases'});
});

module.exports = router;
