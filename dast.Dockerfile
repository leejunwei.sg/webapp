# Custom DAST Image based off (https://gitlab.com/gitlab-org/security-products/dast/-/blob/master/Dockerfile) and (https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/DAST.gitlab-ci.yml)
FROM registry.gitlab.com/gitlab-org/security-products/analyzers/dast:1

USER root

RUN apt-get update && apt-get install --assume-yes --no-install-recommends mysql-client && apt-get clean && rm -rf /var/lib/apt/lists/*

USER zap
WORKDIR /output

ENTRYPOINT []
CMD ["/analyze"]

