// %%*
// Anything between this quote will be read-only
// *%%

// %% This line is read-only

#include <stdio.h>

int main ()
{
    printf("Hello, World!");
    return 0;
}