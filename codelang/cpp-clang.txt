// %%*
// Anything between this quote will be read-only
// *%%

// %% This line is read-only

#include <iostream>

using namespace std;

int main ()
{
    cout << "Hello World!" << endl;
    return 0;
}